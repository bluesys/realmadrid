(function($) {
	
	$('#block-views-block-slider-block-1 ul').slick({
	  dots: false,
	  infinite: true,
	  speed: 100,
	  slidesToShow: 1,
	  centerMode: true,
	  variableWidth: true,
	  autoplay: true,
	  arrows: false
	});
}(jQuery));